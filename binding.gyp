{
    'targets': [
        {
            'target_name': 'testtools',

            'sources': [ 
                'testtools.cc',
                'systemmonitor_p.h',
                'systemmonitor.h', 
                'systemmonitor.cc',
                'processmonitor_p.h',
                'processmonitor.h',
                'processmonitor.cc'
            ],

            'cflags': [
                '-std=c++11',
                '-stdlib=libc++'
            ],

            'conditions': [
                ['OS == "win"',
                    {
                        'defines': [
                            'TT_WIN32'
                        ],

                        'sources': [
                            'systemmonitor_win32.cc',
                            'processmonitor_win32.cc'
                        ],

                        'libraries': [
                            '-lpdh',
                            '-lshlwapi'
                        ],

                        'msvs_settings': {
                            'VCCLCompilerTool': {
                                'ExceptionHandling': '2',  # /EHsc
                            },
                        }
                    }
                ],
                
                ['OS == "linux"', 
                    {
                        'defines': [
                            'TT_LINUX'
                        ],

                        'sources': [
                            'systemmonitor_linux.cc',
                            'processmonitor_linux.cc'
                        ]
                    }
                ]
            ]
        }
    ]
}
