
#include "processmonitor.h"
#include "processmonitor_p.h"

#include <exception>
#include <system_error>
#include <array>
#include <vector>
#include <string>

using v8::Isolate;
using v8::HandleScope;
using v8::Persistent;
using v8::Function;
using v8::FunctionTemplate;
using v8::FunctionCallbackInfo;
using v8::PropertyAttribute;
using v8::Context;
using v8::Value;
using v8::Local;
using v8::Object;
using v8::Array;
using v8::String;
using v8::Int32;
using v8::Uint32;
using v8::Exception;

using std::invalid_argument;
using std::system_error;
using std::array;
using std::vector;
using std::string;

namespace testtools
{

Persistent<Function> ProcessMonitor::constructor_;

// ���������� ���������� �� ������� �������� (��, ��������, ��������)
array<ProcessMonitor::CounterInfo, ProcessMonitor::kCountersNumber> const ProcessMonitor::info_ =
{{
    {
        UPTIME,
        L"����� ����������",
        L"����� ����� ���������� �������� (� ��������)."
    },
    {
        HANDLES,
        L"���������� ������������",
        L"����� ���������� ������������, �������� �� ������ �������."
    },
    {
        THREADS,
        L"���������� �������",
        L"���������� ������� ������� ��������, �������� �� ������ �������."
    },
    {
        PRIORITYBASE,
        L"������� ���������",
        L"������� ������� ��������� ��������."
    },
    {
        PROCESSORTIME,
        L"������� �������� ����������",
        L"���������� ��������� ������� ����������, ����������� ������ ��������� �� ���������� ����������. "
    },
    {
        MEMORYUSAGEPERCENT,
        L"������� ������������� ���. ������",
        L"������� ������������� ��������� ���������� ������ �� ������ �������."
    },
    {
        MEMORYUSAGEBYTES,
        L"���������� ������������ ���. ������",
        L"���������� ������������ ��������� ���������� ������ (����) �� ������ �������."
    },
    {
        VIRTMEMORYUSAGEPERCENT,
        L"������� ������������� ����. ������",
        L"������� ������������� ��������� ����������� ������ �� ������ �������."
    },
    {
        VIRTMEMORYUSAGEBYTES,
        L"���������� ������������ ����. ������",
        L"���������� ������������ ��������� ����. ������ (����) �� ������ �������."
    },
    {
        SWAPUSAGEPERCENT,
        L"������� ������������� ����� ��������",
        L"������� ������������� ��������� ����� �������� �� ������ �������."
    },
    {
        SWAPUSAGEBYTES,
        L"���������� ������������� ����� � ����� ��������",
        L"���������� ������������� ��������� ����� � ����� �������� (����) �� ������ �������."
    },
    {
        IOOPERATIONSSEC,
        L"���������� �������� � ������� � �������",
        L"��������, � ������� ������� ��������� �������� ������ � ������ ��� �����/������."
    },
    {
        IOBYTESSEC,
        L"�������� ������ ������� � �������",
        L"��������, � ������� ������� ��������� ������ � ������ ��� �����/������."
    }
}};

ProcessMonitor::ProcessMonitor(int32_t pid)
    : impl(new ProcessMonitorPrivate(pid))
{

}

ProcessMonitor::~ProcessMonitor()
{
    delete impl;
}

void ProcessMonitor::Initialize(Local<Object> exports)
{
    Isolate* isolate = Isolate::GetCurrent();

    Local<FunctionTemplate> t = FunctionTemplate::New(isolate, New);
    t->SetClassName(String::NewFromUtf8(isolate, kClassName));
    t->InstanceTemplate()->SetInternalFieldCount(1);

    NODE_SET_METHOD(t, "processes", GetProcesses);
    NODE_SET_METHOD(t, "counters", GetCounters);

    NODE_SET_PROTOTYPE_METHOD(t, "query", Query);
    
    t->Set(String::NewFromUtf8(isolate, "UPTIME"), Uint32::NewFromUnsigned(isolate, UPTIME),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "HANDLES"), Uint32::NewFromUnsigned(isolate, HANDLES),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "THREADS"), Uint32::NewFromUnsigned(isolate, THREADS),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "PRIORITYBASE"), Uint32::NewFromUnsigned(isolate, PRIORITYBASE),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "PROCESSORTIME"), Uint32::NewFromUnsigned(isolate, PROCESSORTIME),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "MEMORYUSAGEPERCENT"), Uint32::NewFromUnsigned(isolate, MEMORYUSAGEPERCENT),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "MEMORYUSAGEBYTES"), Uint32::NewFromUnsigned(isolate, MEMORYUSAGEBYTES),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "VIRTMEMORYUSAGEPERCENT"), Uint32::NewFromUnsigned(isolate, VIRTMEMORYUSAGEPERCENT),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "VIRTMEMORYUSAGEBYTES"), Uint32::NewFromUnsigned(isolate, VIRTMEMORYUSAGEBYTES),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "SWAPUSAGEPERCENT"), Uint32::NewFromUnsigned(isolate, SWAPUSAGEPERCENT),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "SWAPUSAGEBYTES"), Uint32::NewFromUnsigned(isolate, SWAPUSAGEBYTES),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "IOOPERATIONSSEC"), Uint32::NewFromUnsigned(isolate, IOOPERATIONSSEC),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "IOBYTESSEC"), Uint32::NewFromUnsigned(isolate, IOBYTESSEC),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));
    t->Set(String::NewFromUtf8(isolate, "ALLCOUNTERS"), Uint32::NewFromUnsigned(isolate, ALLCOUNTERS),
        static_cast<PropertyAttribute>(v8::ReadOnly | v8::DontDelete));

    constructor_.Reset(isolate, t->GetFunction());

    exports->Set(String::NewFromUtf8(isolate, kClassName), t->GetFunction());
}

void ProcessMonitor::New(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    if (args.IsConstructCall() == true) {
        try {
            if (args.Length() != 1) {
                throw invalid_argument("Invalid number of parameters!");
            } 
            
            if (args[0]->IsInt32() == false) {
                throw invalid_argument("Parameter \"pid\" is not a integer!");
            }

            const int32_t pid = args[0]->Int32Value();

            ProcessMonitor* instance = new ProcessMonitor(pid);
            instance->Wrap(args.This());

            args.GetReturnValue().Set(args.This());
        }
        catch (const invalid_argument& e) {
            isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, e.what())));
        }
        catch (const system_error& e) {
            const char* code = std::to_string(e.code().value()).data();
            isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, code)));
        }
    }
    else {
        const uint8_t argc = 1;
        Local<Value> argv[argc] = { args[0] };
        Local<Context> ctx = isolate->GetCurrentContext();
        Local<Function> ctor = Local<Function>::New(isolate, constructor_);
        Local<Object> instance = ctor->NewInstance(ctx, argc, argv).ToLocalChecked();

        args.GetReturnValue().Set(instance);
    }
}

void ProcessMonitor::GetCounters(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    Local<Array> result = Array::New(isolate, info_.size());
    for (size_t i = 0; i < info_.size(); ++i) {
        Local<Object> ci = Object::New(isolate);
        ci->Set(String::NewFromUtf8(isolate, "id"),
            Uint32::NewFromUnsigned(isolate, info_[i].id));
        ci->Set(String::NewFromUtf8(isolate, "name"),
            String::NewFromTwoByte(isolate, reinterpret_cast<uint16_t*>(info_[i].name)));
        ci->Set(String::NewFromUtf8(isolate, "title"),
            String::NewFromTwoByte(isolate, reinterpret_cast<uint16_t*>(info_[i].title)));

        result->Set(i, ci);
    }

    args.GetReturnValue().Set(result);
}

void ProcessMonitor::GetProcesses(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    vector<Process> list = ProcessMonitorPrivate::GetProcesses();
    Local<Array> result = Array::New(isolate, list.size());
    for (size_t i = 0; i < list.size(); ++i) {
        Local<Object> proc = Object::New(isolate);
        proc->Set(String::NewFromUtf8(isolate, "pid"),
            Int32::New(isolate, list[i].pid));
        proc->Set(String::NewFromUtf8(isolate, "name"),
            String::NewFromUtf8(isolate, list[i].name.data()));

        result->Set(i, proc);
    }

    args.GetReturnValue().Set(result);
}

void ProcessMonitor::Query(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    try {
        if (args.Length() != 2) {
            throw invalid_argument("Invalid number of parameters!");
        }

        if (args[0]->IsUint32() == false) {
            throw std::invalid_argument("Parameter \"counters\" is not a integer!");
        }

        if (args[1]->IsFunction() == false) {
            throw std::invalid_argument("Parameter \"callback\" is not a function!");
        }
        
        const uint32_t counters = args[0]->Uint32Value();
        Local<Function> cb = Local<Function>::Cast(args[1]);

        QueryWork* qw = new QueryWork();
        qw->request.data = qw;
        qw->callback.Reset(isolate, cb);
        qw->instance = ObjectWrap::Unwrap<ProcessMonitor>(args.Holder());
        qw->counters = counters;

        uv_queue_work(uv_default_loop(), &qw->request, QueryAsync, QueryAsyncAfter);
    }
    catch (const std::invalid_argument& e) {
        isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, e.what())));
    }
}

void ProcessMonitor::QueryAsync(uv_work_t* req)
{
    QueryWork* qw = static_cast<QueryWork*>(req->data);
    if (qw == nullptr) {
        uv_cancel(req->next_req);
        return;
    }

    try {
        qw->instance->impl->ExecQuery();
    }
    catch (const system_error& e) {
        printf("Error occured with code: %lu\n", e.code().value());
        uv_cancel(req->next_req);
    }
}

void ProcessMonitor::QueryAsyncAfter(uv_work_t* req, int status) {
    if (status == UV_ECANCELED) return;

    QueryWork* qw = static_cast<QueryWork*>(req->data);
    if (qw == nullptr) return;

    ProcessMonitorPrivate* const impl = qw->instance->impl;

    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    Local<Object> data = Object::New(isolate);
    if ((qw->counters & UPTIME) != 0) {
        data->Set(String::NewFromUtf8(isolate, "upTime"),
            Uint32::New(isolate, impl->GetUpTime()));
    }

    if ((qw->counters & HANDLES) != 0) {
        data->Set(String::NewFromUtf8(isolate, "handles"),
            Uint32::New(isolate, impl->GetHandles()));
    }

    if ((qw->counters & THREADS) != 0) {
        data->Set(String::NewFromUtf8(isolate, "threads"),
            Uint32::New(isolate, impl->GetThreads()));
    }

    if ((qw->counters & PRIORITYBASE) != 0) {
        data->Set(String::NewFromUtf8(isolate, "priorityBase"),
            Uint32::New(isolate, impl->GetPriorityBase()));
    }

    if ((qw->counters & PROCESSORTIME) != 0) {
        data->Set(String::NewFromUtf8(isolate, "processorTime"),
            Uint32::New(isolate, impl->GetProcessorTime()));
    }

    if ((qw->counters & MEMORYUSAGEPERCENT) != 0) {
        data->Set(String::NewFromUtf8(isolate, "memoryUsagePercent"),
            Uint32::New(isolate, impl->GetMemoryUsagePercent()));
    }

    if ((qw->counters & MEMORYUSAGEBYTES) != 0) {
        data->Set(String::NewFromUtf8(isolate, "memoryUsageBytes"),
            Uint32::New(isolate, impl->GetMemoryUsageBytes()));
    }

    if ((qw->counters & VIRTMEMORYUSAGEPERCENT) != 0) {
        data->Set(String::NewFromUtf8(isolate, "virtMemoryUsagePercent"),
            Uint32::New(isolate, impl->GetVirtMemoryUsagePercent()));
    }

    if ((qw->counters & VIRTMEMORYUSAGEBYTES) != 0) {
        data->Set(String::NewFromUtf8(isolate, "virtMemoryUsageBytes"),
            Uint32::New(isolate, impl->GetVirtMemoryUsageBytes()));
    }

    if ((qw->counters & SWAPUSAGEPERCENT) != 0) {
        data->Set(String::NewFromUtf8(isolate, "swapUsagePercent"),
            Uint32::New(isolate, impl->GetSwapUsagePercent()));
    }

    if ((qw->counters & SWAPUSAGEBYTES) != 0) {
        data->Set(String::NewFromUtf8(isolate, "swapUsageBytes"),
            Uint32::New(isolate, impl->GetSwapUsageBytes()));
    }

    if ((qw->counters & IOOPERATIONSSEC) != 0) {
        data->Set(String::NewFromUtf8(isolate, "ioOperationsSec"),
            Uint32::New(isolate, impl->GetIOOperationsSec()));
    }

    if ((qw->counters & IOBYTESSEC) != 0) {
        data->Set(String::NewFromUtf8(isolate, "ioBytesSec"),
            Uint32::New(isolate, impl->GetIOBytesSec()));
    }

    const uint8_t argc = 1;
    Local<Value> argv[argc] = { data };
    Local<Function> cb = Local<Function>::New(isolate, qw->callback);
    cb->Call(isolate->GetCurrentContext()->Global(), argc, argv);

    qw->callback.Reset();

    delete qw;
    qw = nullptr;
}

} // namespace testtools
