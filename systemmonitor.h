
#pragma once

#ifndef TESTTOOLS_SYSTEMMONITOR_H
#define TESTTOOLS_SYSTEMMONITOR_H

#include <array>

#include <uv.h>
#include <v8.h>
#include <node.h>
#include <node_object_wrap.h>

namespace testtools
{

class SystemMonitorPrivate;
class SystemMonitor
{
    friend class SystemMonitorPrivate;

// ����
public:
    // ��������� ��� �������� ���������� � ��������
    typedef struct
    {
        uint32_t id;
        wchar_t* name;
        wchar_t* title;
    } CounterInfo;

private:
    // ��������� ��� ���������� ����������� ������ ������ Query
    typedef struct
    {
        uv_work_t request;
        v8::Persistent<v8::Function> callback;
        uint32_t counters;
    } QueryWork;

// ������������
private:
    SystemMonitor() = delete;
    ~SystemMonitor() = delete;

    SystemMonitor(const SystemMonitor& other) = delete;
    SystemMonitor(const SystemMonitor&& other) = delete;

// ���������
private:
    SystemMonitor& operator=(const SystemMonitor& rhs) = delete;

// ������
public:
    static void Initialize(v8::Local<v8::Object> exports);

private:
    static void GetCounters(const v8::FunctionCallbackInfo<v8::Value>& args);

    static void Query(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void QueryAsync(uv_work_t* req);
    static void QueryAsyncAfter(uv_work_t* req, int status);

// ���������
public:
    // ������� ����� ��� ������� ���������� ��������� � JavaScript
    static const uint16_t UPTIME = 1;
    static const uint16_t PROCESSES = 2;
    static const uint16_t THREADS = 4;
    static const uint16_t PROCESSORTIME = 8;
    static const uint16_t PROCESSORINTERRUPTSSEC = 16;
    static const uint16_t MEMORYUSAGEPERCENT = 32;
    static const uint16_t MEMORYUSAGEBYTES = 64;
    static const uint16_t VIRTMEMORYUSAGEPERCENT = 128;
    static const uint16_t VIRTMEMORYUSAGEBYTES = 256;
    static const uint16_t SWAPUSAGEPERCENT = 512;
    static const uint16_t SWAPUSAGEBYTES = 1024;
    static const uint16_t DISKTIME = 2048;
    static const uint16_t DISKBYTESSEC = 4096;
    static const uint16_t DISKTRANSFERSSEC = 8192;
    static const uint16_t ALLCOUNTERS = 16383; // ��� �������� = ����� ���� ��������� ������� �����

    // ���������� �������������� ���������
    static const uint8_t kCountersNumber = 14;

private:
    // �������� ������ � JavaScript
    static constexpr const char* const kClassName = "SystemMonitor";

    // ������ ���������� ���������� �� ������� �������� (��, ��������, ��������)
    static std::array<CounterInfo, kCountersNumber> const info_;
};

} // namespace testtools

#endif // TESTTOOLS_SYSTEMMONITOR_H
