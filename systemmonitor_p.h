
#pragma once

#ifndef TESTTOOLS_SYSTEMMONITOR_PRIVATE_H
#define TESTTOOLS_SYSTEMMONITOR_PRIVATE_H

#ifdef TT_WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <pdh.h>
#include <pdhmsg.h>

#endif // TT_WIN32

#include <cstdint>
#include <string>

namespace testtools
{

class SystemMonitorPrivate
{
// ������������
private:
    SystemMonitorPrivate();
    ~SystemMonitorPrivate();

    SystemMonitorPrivate(const SystemMonitorPrivate& other) = delete;
    SystemMonitorPrivate(const SystemMonitorPrivate&& other) = delete;

// ���������
private:
    SystemMonitorPrivate& operator=(const SystemMonitorPrivate& rhs) = delete;

// ������
public:
    inline static SystemMonitorPrivate* GetInstance() {
        static SystemMonitorPrivate instance;
        return &instance;
    }

public:
    void ExecQuery();

    uint32_t GetUpTime() const;
    uint32_t GetProcesses() const;
    uint32_t GetThreads() const;

    uint8_t GetProcessorTime() const;
    uint32_t GetProcessorInterruptsSec() const;

    uint8_t GetMemoryUsagePercent() const;
    uint32_t GetMemoryUsageBytes() const;

    uint8_t GetVirtMemoryUsagePercent() const;
    uint32_t GetVirtMemoryUsageBytes() const;

    uint8_t GetSwapUsagePercent() const;
    uint32_t GetSwapUsageBytes() const;

    uint8_t GetDiskTime() const;
    uint32_t GetDiskBytesSec() const;
    uint32_t GetDiskTransfersSec() const;

// ����������
public:
#ifdef TT_WIN32
    PDH_HQUERY systemQuery_;

    PDH_HCOUNTER upTimeCounter_;
    PDH_HCOUNTER processesCounter_;
    PDH_HCOUNTER threadsCounter_;
    PDH_HCOUNTER processorTimeCounter_;
    PDH_HCOUNTER processorInterruptsSecCounter_;
    PDH_HCOUNTER diskTimeCounter_;
    PDH_HCOUNTER diskBytesSecCounter_;
    PDH_HCOUNTER diskTransfersSecCounter_;

    MEMORYSTATUSEX memStatus_;
#endif // TT_WIN32

// ���������
public:
#ifdef TT_WIN32
    // ���� � ��������� �������
    static constexpr const char* const kUpTimePath
        = "\\System\\System Up Time";
    static constexpr const char* const kProcessesPath
        = "\\System\\Processes";
    static constexpr const char* const kThreadsPath
        = "\\System\\Threads";
    static constexpr const char* const kProcessorTimePath
        = "\\Processor(_Total)\\% Processor Time";
    static constexpr const char* const kProcessorInterruptsSecPath
        = "\\Processor(_Total)\\Interrupts/sec";
    static constexpr const char* const kDiskTimePath
        = "\\PhysicalDisk(_Total)\\% Disk Time";
    static constexpr const char* const kDiskBytesSecPath
        = "\\PhysicalDisk(_Total)\\Disk Bytes/sec";
    static constexpr const char* const kDiskTransfersSecPath
        = "\\PhysicalDisk(_Total)\\Disk Transfers/sec";
#endif // TT_WIN32
};

} // namespace testtools

#endif // TESTTOOLS_SYSTEMMONITOR_PRIVATE_H