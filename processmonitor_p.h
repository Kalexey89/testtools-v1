
#pragma once

#ifndef TESTTOOLS_PROCESSMONITOR_PRIVATE_H
#define TESTTOOLS_PROCESSMONITOR_PRIVATE_H

#ifdef TT_WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <psapi.h>
#include <shlwapi.h>
#include <tlhelp32.h>
#include <pdh.h>
#include <pdhmsg.h>

#endif // TT_WIN32

#include <cstdint>
#include <vector>
#include <string>

namespace testtools
{

typedef struct
{
    int32_t pid;
    std::string name;
} Process;

class ProcessMonitorPrivate
{
// ������������
public:
    explicit ProcessMonitorPrivate(int32_t pid);
    ~ProcessMonitorPrivate();

    ProcessMonitorPrivate(const ProcessMonitorPrivate& other) = delete;
    ProcessMonitorPrivate(const ProcessMonitorPrivate&& other) = delete;

// ���������
public:
    ProcessMonitorPrivate& operator=(const ProcessMonitorPrivate& rhs) = delete;

// ������
public:
    static std::vector<Process> GetProcesses();

    void ExecQuery();

    uint32_t GetUpTime() const;
    uint32_t GetHandles() const;
    uint32_t GetThreads() const;

    uint32_t GetPriorityBase() const;

    uint8_t GetProcessorTime() const;

    uint8_t GetMemoryUsagePercent() const;
    uint32_t GetMemoryUsageBytes() const;

    uint8_t GetVirtMemoryUsagePercent() const;
    uint32_t GetVirtMemoryUsageBytes() const;

    uint8_t GetSwapUsagePercent() const;
    uint32_t GetSwapUsageBytes() const;

    uint32_t GetIOOperationsSec() const;
    uint32_t GetIOBytesSec() const;

#ifdef TT_WIN32
    static std::string GetProcessNameByPid(int32_t pid);
    static std::string GetProcessCounterPath(const std::string& process, const std::string& counter);
#endif // TT_WIN32

// ����������
public:
#ifdef TT_WIN32
    PDH_HQUERY processQuery_;

    PDH_HCOUNTER upTimeCounter_;
    PDH_HCOUNTER handlesCounter_;
    PDH_HCOUNTER threadsCounter_;
    PDH_HCOUNTER priorityBaseCounter_;
    PDH_HCOUNTER processorTimeCounter_;
    PDH_HCOUNTER memoryCounter_;
    PDH_HCOUNTER virtMemoryCounter_;
    PDH_HCOUNTER swapCounter_;
    PDH_HCOUNTER ioOperationsSecCounter_;
    PDH_HCOUNTER ioBytesSecCounter_;

    MEMORYSTATUSEX memoryStatus_;
#endif // TT_WIN32

// ���������
public:
#ifdef TT_WIN32
    // ���� � ��������� ��������
    static constexpr const char* const kUpTime = "Elapsed Time";
    static constexpr const char* const kHandles = "Handle Count";
    static constexpr const char* const kThreads = "Thread Count";
    static constexpr const char* const kPriorityBase = "Priority Base";
    static constexpr const char* const kProcessorTime = "% Processor Time";
    static constexpr const char* const kMemory = "Working Set - Private";
    static constexpr const char* const kVirtMemory = "Private Bytes";
    static constexpr const char* const kSwap = "Page File Bytes";
    static constexpr const char* const kIOOperationsSec = "IO Data Operations/sec";
    static constexpr const char* const kIOBytesSec = "IO Data Bytes/sec";
#endif // TT_WIN32
};

} // namespace testtools

#endif // TESTTOOLS_PROCESSMONITOR_PRIVATE_H
