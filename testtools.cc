
#include "systemmonitor.h"
#include "processmonitor.h"

#include <v8.h>
#include <node.h>

using v8::Local;
using v8::Object;

namespace testtools
{

void Initialize(Local<Object> exports)
{
    SystemMonitor::Initialize(exports);
    ProcessMonitor::Initialize(exports);
}

NODE_MODULE(testtools, Initialize);

} // namespace testtools
