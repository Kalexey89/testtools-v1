#include "systemmonitor_p.h"

#include <string>
#include <exception>
#include <system_error>
#include <cmath>

using std::string;
using std::system_error;
using std::system_category;

namespace testtools
{

SystemMonitorPrivate::SystemMonitorPrivate()
    : systemQuery_(nullptr)
    , upTimeCounter_(nullptr)
    , processesCounter_(nullptr)
    , threadsCounter_(nullptr)
    , processorTimeCounter_(nullptr)
    , processorInterruptsSecCounter_(nullptr)
    , diskTimeCounter_(nullptr)
    , diskBytesSecCounter_(nullptr)
    , diskTransfersSecCounter_(nullptr)
    , memStatus_({ 0 })
{
    PDH_STATUS error = ERROR_SUCCESS;

    error = ::PdhOpenQueryA(nullptr, 0, &systemQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kUpTimePath, 0, &upTimeCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }
    
    error = ::PdhAddEnglishCounterA(systemQuery_, kProcessesPath, 0, &processesCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kThreadsPath, 0, &threadsCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kProcessorTimePath, 0, &processorTimeCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kProcessorInterruptsSecPath, 0, &processorInterruptsSecCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kDiskTimePath, 0, &diskTimeCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kDiskBytesSecPath, 0, &diskBytesSecCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhAddEnglishCounterA(systemQuery_, kDiskTransfersSecPath, 0, &diskTransfersSecCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhCollectQueryData(systemQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }
}

SystemMonitorPrivate::~SystemMonitorPrivate()
{
    if (systemQuery_ != nullptr) {
        ::PdhCloseQuery(systemQuery_);
        systemQuery_ = nullptr;
    }
}

void SystemMonitorPrivate::ExecQuery()
{
    PDH_STATUS error = ::PdhCollectQueryData(systemQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }
        
    ::ZeroMemory(&memStatus_, sizeof(MEMORYSTATUSEX));
    memStatus_.dwLength = sizeof(MEMORYSTATUSEX);
    if (::GlobalMemoryStatusEx(&memStatus_) == FALSE) {
        throw system_error(::GetLastError(), system_category());
    }
    
}

uint32_t SystemMonitorPrivate::GetUpTime() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(upTimeCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t SystemMonitorPrivate::GetProcesses() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(processesCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t SystemMonitorPrivate::GetThreads() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(threadsCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint8_t SystemMonitorPrivate::GetProcessorTime() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(processorTimeCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
       //throw system_error(error, system_category());
    }

    return static_cast<uint8_t>(value.longValue);
}

uint32_t SystemMonitorPrivate::GetProcessorInterruptsSec() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(processorInterruptsSecCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint8_t SystemMonitorPrivate::GetMemoryUsagePercent() const
{
    uint64_t total = static_cast<uint64_t>(memStatus_.ullTotalPhys);
    uint64_t free = static_cast<uint64_t>(memStatus_.ullAvailPhys);

    return static_cast<uint8_t>(rint(((total - free) * 100) / total));
}

uint32_t SystemMonitorPrivate::GetMemoryUsageBytes() const
{
    return static_cast<uint32_t>(memStatus_.ullTotalPhys - memStatus_.ullAvailPhys);
}

uint8_t SystemMonitorPrivate::GetVirtMemoryUsagePercent() const
{
    uint64_t total = static_cast<uint64_t>(memStatus_.ullTotalVirtual);
    uint64_t free = static_cast<uint64_t>(memStatus_.ullAvailVirtual);

    return static_cast<uint8_t>(rint(((total - free) * 100) / total));
}

uint32_t SystemMonitorPrivate::GetVirtMemoryUsageBytes() const
{
    return static_cast<uint32_t>(memStatus_.ullTotalVirtual - memStatus_.ullAvailVirtual);
}

uint8_t SystemMonitorPrivate::GetSwapUsagePercent() const
{
    uint64_t total = static_cast<uint64_t>(memStatus_.ullTotalPageFile);
    uint64_t free = static_cast<uint64_t>(memStatus_.ullAvailPageFile);

    return static_cast<uint8_t>(rint(((total - free) * 100) / total));
}

uint32_t SystemMonitorPrivate::GetSwapUsageBytes() const
{
    return static_cast<uint32_t>(memStatus_.ullTotalPageFile - memStatus_.ullAvailPageFile);
}

uint8_t SystemMonitorPrivate::GetDiskTime() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(diskTimeCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t SystemMonitorPrivate::GetDiskBytesSec() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(diskBytesSecCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t SystemMonitorPrivate::GetDiskTransfersSec() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(diskTransfersSecCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

} // namespace testtools
