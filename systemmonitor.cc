
#include "systemmonitor.h"
#include "systemmonitor_p.h"

#include <array>
#include <exception>
#include <system_error>

using v8::Isolate;
using v8::HandleScope;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::Context;
using v8::Value;
using v8::Local;
using v8::Array;
using v8::Object;
using v8::String;
using v8::Uint32;
using v8::Exception;

using std::array;
using std::system_error;
using std::invalid_argument;

namespace testtools
{

// ���������� ���������� �� ������� �������� (��, ��������, ��������)
array<SystemMonitor::CounterInfo, SystemMonitor::kCountersNumber> const SystemMonitor::info_ =
{{
    {
        UPTIME,
        L"����� ������",
        L"����� ������ ������� (� ��������) � ������� �������"
    },
    {
        PROCESSES,
        L"���������� ���������",
        L"���������� ��������� �� ������ �������"
    },
    {
        THREADS,
        L"���������� �������",
        L"���������� ������� �� ������ �������"
    },
    {
        PROCESSORTIME,
        L"������� �������� ����������",
        L"�����, ������� ������ ��������� �� ��������� ���� ������� ������. ���������� ������� �������� � ������� ��������� �������."
    },
    {
        PROCESSORINTERRUPTSSEC,
        L"���������� ���������� � �������",
        L"������� ��������, � �������� � �������, � ������� ��������� ����������� ���������� ����������."
    },
    {
        MEMORYUSAGEPERCENT,
        L"������� ������������� ���. ������",
        L"������� ������������� ���������� ������ �� ������ �������."
    },
    {
        MEMORYUSAGEBYTES,
        L"���������� ������������ ���. ������",
        L"���������� ������������ ���������� ������ (����) �� ������ �������."
    },
    {
        VIRTMEMORYUSAGEPERCENT,
        L"������� ������������� ����. ������",
        L"������� ������������� ����������� ������ �� ������ �������."
    },
    {
        VIRTMEMORYUSAGEBYTES,
        L"���������� ������������ ����. ������",
        L"���������� ������������ ����. ������ (����) �� ������ �������."
    },
    {
        SWAPUSAGEPERCENT,
        L"������� ������������� ����� ��������",
        L"������� ������������� ����� �������� �� ������ �������."
    },
    {
        SWAPUSAGEBYTES,
        L"���������� ������������� ����� � ����� ��������",
        L"���������� ������������� ����� � ����� �������� (����) �� ������ �������."
    },
    {
        DISKTIME,
        L"������� ���������� �����",
        L"������� �������, ������������ ������ �� ��������� �������� ������ � ������ ������."
    },
    {
        DISKBYTESSEC,
        L"�������� ������ � ������ ����/���",
        L"��������, � ������� ���������� ����� � ������ ��� ��������� ������ � ������."
    },
    {
        DISKTRANSFERSSEC,
        L"��������� � ����� � �������",
        L"������� ���������� �������� ������ � ������ �� ����."
    }
}};

void SystemMonitor::Initialize(Local<Object> exports)
{
    Isolate* isolate = Isolate::GetCurrent();

    Local<Object> t = Object::New(isolate);
    NODE_SET_METHOD(t, "counters", GetCounters);
    NODE_SET_METHOD(t, "query", Query);

    NODE_DEFINE_CONSTANT(t, UPTIME);
    NODE_DEFINE_CONSTANT(t, PROCESSES);
    NODE_DEFINE_CONSTANT(t, THREADS);
    NODE_DEFINE_CONSTANT(t, PROCESSORTIME);
    NODE_DEFINE_CONSTANT(t, PROCESSORINTERRUPTSSEC);
    NODE_DEFINE_CONSTANT(t, MEMORYUSAGEPERCENT);
    NODE_DEFINE_CONSTANT(t, MEMORYUSAGEBYTES);
    NODE_DEFINE_CONSTANT(t, VIRTMEMORYUSAGEPERCENT);
    NODE_DEFINE_CONSTANT(t, VIRTMEMORYUSAGEBYTES);
    NODE_DEFINE_CONSTANT(t, SWAPUSAGEPERCENT);
    NODE_DEFINE_CONSTANT(t, SWAPUSAGEBYTES);
    NODE_DEFINE_CONSTANT(t, DISKTIME);
    NODE_DEFINE_CONSTANT(t, DISKBYTESSEC);
    NODE_DEFINE_CONSTANT(t, DISKTRANSFERSSEC);
    NODE_DEFINE_CONSTANT(t, ALLCOUNTERS);

    exports->Set(String::NewFromUtf8(isolate, kClassName), t);
}

void SystemMonitor::GetCounters(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    Local<Array> result = Array::New(isolate, info_.size());
    for (size_t i = 0; i < info_.size(); ++i) {
        Local<Object> ci = Object::New(isolate);
        ci->Set(String::NewFromUtf8(isolate, "id"),
            Uint32::NewFromUnsigned(isolate, info_[i].id));
        ci->Set(String::NewFromUtf8(isolate, "name"),
            String::NewFromTwoByte(isolate, reinterpret_cast<uint16_t*>(info_[i].name)));
        ci->Set(String::NewFromUtf8(isolate, "title"),
            String::NewFromTwoByte(isolate, reinterpret_cast<uint16_t*>(info_[i].title)));

        result->Set(i, ci);
    }

    args.GetReturnValue().Set(result);
}

void SystemMonitor::Query(const FunctionCallbackInfo<Value>& args)
{
    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);

    try {
        if (args.Length() != 2) {
            throw invalid_argument("Invalid number of parameters!");
        }

        if (args[0]->IsUint32() == false) {
            throw invalid_argument("Parameter \"counters\" is not a integer!");
        }

        if (args[1]->IsFunction() == false) {
            throw invalid_argument("Parameter \"callback\" is not a function!");
        }

        const uint32_t counters = args[0]->Uint32Value();
        Local<Function> cb = Local<Function>::Cast(args[1]);

        QueryWork* qw = new QueryWork();
        qw->request.data = qw;
        qw->callback.Reset(isolate, cb);
        qw->counters = counters;

        uv_queue_work(uv_default_loop(), &qw->request, QueryAsync, QueryAsyncAfter);
    }
    catch (const invalid_argument& e) {
        isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, e.what())));
    }
}

void SystemMonitor::QueryAsync(uv_work_t* req)
{
    try {
        SystemMonitorPrivate::GetInstance()->ExecQuery();
    }
    catch (const system_error& e) {
        printf("Error occured with code: %d\n", e.code().value());
        uv_cancel(req->next_req);
    }
}

void SystemMonitor::QueryAsyncAfter(uv_work_t* req, int status)
{
    if (status == UV_ECANCELED) return;

    QueryWork* qw = static_cast<QueryWork*>(req->data);
    if (qw == nullptr) return;

    Isolate* isolate = Isolate::GetCurrent();
    HandleScope scope(isolate);
    
    try {
        SystemMonitorPrivate* const impl = SystemMonitorPrivate::GetInstance();

        Local<Object> data = Object::New(isolate);
        if ((qw->counters & UPTIME) != 0) {
            data->Set(String::NewFromUtf8(isolate, "upTime"),
                Uint32::NewFromUnsigned(isolate, impl->GetUpTime()));
        }

        if ((qw->counters & PROCESSES) != 0) {
            data->Set(String::NewFromUtf8(isolate, "processes"),
                Uint32::NewFromUnsigned(isolate, impl->GetProcesses()));
        }

        if ((qw->counters & THREADS) != 0) {
            data->Set(String::NewFromUtf8(isolate, "threads"),
                Uint32::NewFromUnsigned(isolate, impl->GetThreads()));
        }
        
        if ((qw->counters & PROCESSORTIME) != 0) {
            data->Set(String::NewFromUtf8(isolate, "processorTime"),
                Uint32::NewFromUnsigned(isolate, impl->GetProcessorTime()));
        }

        if ((qw->counters & PROCESSORINTERRUPTSSEC) != 0) {
            data->Set(String::NewFromUtf8(isolate, "processorInterruptsSec"),
                Uint32::NewFromUnsigned(isolate, impl->GetProcessorInterruptsSec()));
        }

        if ((qw->counters & MEMORYUSAGEPERCENT) != 0) {
            data->Set(String::NewFromUtf8(isolate, "memoryUsagePercent"),
                Uint32::NewFromUnsigned(isolate, impl->GetMemoryUsagePercent()));
        }

        if ((qw->counters & MEMORYUSAGEBYTES) != 0) {
            data->Set(String::NewFromUtf8(isolate, "memoryUsageBytes"),
                Uint32::NewFromUnsigned(isolate, impl->GetMemoryUsageBytes()));
        }

        if ((qw->counters & VIRTMEMORYUSAGEPERCENT) != 0) {
            data->Set(String::NewFromUtf8(isolate, "virtMemoryUsagePercent"),
                Uint32::NewFromUnsigned(isolate, impl->GetVirtMemoryUsagePercent()));
        }

        if ((qw->counters & VIRTMEMORYUSAGEBYTES) != 0) {
            data->Set(String::NewFromUtf8(isolate, "virtMemoryUsageBytes"),
                Uint32::NewFromUnsigned(isolate, impl->GetVirtMemoryUsageBytes()));
        }

        if ((qw->counters & SWAPUSAGEPERCENT) != 0) {
            data->Set(String::NewFromUtf8(isolate, "swapUsagePercent"),
                Uint32::NewFromUnsigned(isolate, impl->GetSwapUsagePercent()));
        }

        if ((qw->counters & SWAPUSAGEBYTES) != 0) {
            data->Set(String::NewFromUtf8(isolate, "swapUsageBytes"),
                Uint32::NewFromUnsigned(isolate, impl->GetSwapUsageBytes()));
        }

        if ((qw->counters & DISKTIME) != 0) {
            data->Set(String::NewFromUtf8(isolate, "diskTime"),
                Uint32::NewFromUnsigned(isolate, impl->GetDiskTime()));
        }

        if ((qw->counters & DISKBYTESSEC) != 0) {
            data->Set(String::NewFromUtf8(isolate, "diskBytesSec"),
                Uint32::NewFromUnsigned(isolate, impl->GetDiskBytesSec()));
        }

        if ((qw->counters & DISKTRANSFERSSEC) != 0) {
            data->Set(String::NewFromUtf8(isolate, "diskTransfersSec"),
                Uint32::NewFromUnsigned(isolate, impl->GetDiskTransfersSec()));
        }

        const uint8_t argc = 1;
        Local<Value> argv[argc] = { data };
        Local<Function> cb = Local<Function>::New(isolate, qw->callback);
        cb->Call(isolate->GetCurrentContext()->Global(), argc, argv);
    }
    catch (const system_error& e) {
        const char* code = std::to_string(e.code().value()).data();
        isolate->ThrowException(Exception::Error(String::NewFromUtf8(isolate, code)));
    }
        
    qw->callback.Reset();

    delete qw;
    qw = nullptr;
}

} // namespace testtools
