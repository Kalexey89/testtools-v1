
#pragma once

#ifndef TESTTOOLS_PROCESSMONITOR_H
#define TESTTOOLS_PROCESSMONITOR_H

#include <uv.h>
#include <v8.h>
#include <node.h>
#include <node_object_wrap.h>

#include <cstdint>
#include <array>

namespace testtools
{

class ProcessMonitorPrivate;
class ProcessMonitor : public node::ObjectWrap
{
    friend class ProcessMonitorPrivate;

// ����
public:
    // ��������� ��� �������� ���������� � ��������
    typedef struct 
    {
        uint32_t id;
        wchar_t* name;
        wchar_t* title;
    } CounterInfo;

private:
    // ��������� ��� ���������� ����������� ������ ������ Query
    typedef struct
    {
        uv_work_t request;
        v8::Persistent<v8::Function> callback;
        ProcessMonitor* instance;
        uint32_t counters;
    } QueryWork;

// ������������
private:
    explicit ProcessMonitor(int32_t pid);
    ~ProcessMonitor();

    ProcessMonitor(const ProcessMonitor& other) = delete;
    ProcessMonitor(const ProcessMonitor&& other) = delete;

// ���������
private:
    ProcessMonitor& operator=(const ProcessMonitor& rhs) = delete;

// ������
public:
    static void Initialize(v8::Local<v8::Object> exports);

private:
    static void New(const v8::FunctionCallbackInfo<v8::Value>& args);

    static void GetCounters(const v8::FunctionCallbackInfo<v8::Value>& args);

    static void GetProcesses(const v8::FunctionCallbackInfo<v8::Value>& args);

    static void Query(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void QueryAsync(uv_work_t* req);
    static void QueryAsyncAfter(uv_work_t* req, int status);

// ����������
private:
    static v8::Persistent<v8::Function> constructor_;
    
    ProcessMonitorPrivate* const impl;

// ���������
public:
    // ������� ����� ��� ������� ���������� ��������� � JavaScript
    static const uint16_t UPTIME = 1;
    static const uint16_t HANDLES = 2;
    static const uint16_t THREADS = 4;
    static const uint16_t PRIORITYBASE = 8;
    static const uint16_t PROCESSORTIME = 16;
    static const uint16_t MEMORYUSAGEPERCENT = 32;
    static const uint16_t MEMORYUSAGEBYTES = 64;
    static const uint16_t VIRTMEMORYUSAGEPERCENT = 128;
    static const uint16_t VIRTMEMORYUSAGEBYTES = 256;
    static const uint16_t SWAPUSAGEPERCENT = 512;
    static const uint16_t SWAPUSAGEBYTES = 1024;
    static const uint16_t IOOPERATIONSSEC = 2048;
    static const uint16_t IOBYTESSEC = 4096;
    static const uint16_t ALLCOUNTERS = 8191; // ��� �������� = ����� ���� ��������� ������� �����

    // ���������� �������������� ���������
    static const uint8_t kCountersNumber = 13;

private:
    // �������� ������ � JavaScript
    static constexpr const char* const kClassName = "ProcessMonitor";

    // ������ ���������� ���������� �� ������� �������� (��, ��������, ��������)
    static std::array<CounterInfo, kCountersNumber> const info_;
};

} // namespace testtools

#endif // TESTTOOLS_PROCESSMONITOR_H
