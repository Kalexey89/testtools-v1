#include "processmonitor_p.h"

#include <exception>
#include <system_error>
#include <vector>
#include <string>
#include <cmath>

using std::system_error;
using std::system_category;
using std::vector;
using std::string;

namespace testtools
{

ProcessMonitorPrivate::ProcessMonitorPrivate(int32_t pid)
{
    string name = GetProcessNameByPid(pid);
    string path = "";
    PDH_STATUS error = ERROR_SUCCESS;

    error = ::PdhOpenQueryA(nullptr, 0, &processQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kUpTime);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &upTimeCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kHandles);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &handlesCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kThreads);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &threadsCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kPriorityBase);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &priorityBaseCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kProcessorTime);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &processorTimeCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kMemory);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &memoryCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kVirtMemory);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &virtMemoryCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kSwap);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &swapCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kIOOperationsSec);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &ioOperationsSecCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    path = GetProcessCounterPath(name, kIOBytesSec);
    error = ::PdhAddEnglishCounterA(processQuery_, path.data(), 0, &ioBytesSecCounter_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    error = ::PdhCollectQueryData(processQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }
}

ProcessMonitorPrivate::~ProcessMonitorPrivate()
{
    if (processQuery_ != nullptr) {
        ::PdhCloseQuery(processQuery_);
        processQuery_ = nullptr;
    }
}

vector<Process> ProcessMonitorPrivate::GetProcesses()
{
    HANDLE snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (snapshot == INVALID_HANDLE_VALUE) {
        throw system_error(::GetLastError(), system_category());
    }

    PROCESSENTRY32 pe32 = { 0 };
    pe32.dwSize = sizeof(PROCESSENTRY32);
    if (::Process32First(snapshot, &pe32) == FALSE) {
        ::CloseHandle(snapshot);
        throw system_error(::GetLastError(), system_category());
    }

    vector<Process> list;

    do {
        Process p = { 0 };
        p.pid = pe32.th32ProcessID;
        p.name = pe32.szExeFile;

        list.push_back(p);
    } while (::Process32Next(snapshot, &pe32) != FALSE);

    ::CloseHandle(snapshot);

    return list;
}

void ProcessMonitorPrivate::ExecQuery()
{
    PDH_STATUS error = ::PdhCollectQueryData(processQuery_);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    ::ZeroMemory(&memoryStatus_, sizeof(MEMORYSTATUSEX));
    memoryStatus_.dwLength = sizeof(MEMORYSTATUSEX);
    if (::GlobalMemoryStatusEx(&memoryStatus_) == FALSE) {
        throw system_error(::GetLastError(), system_category());
    }
}

uint32_t ProcessMonitorPrivate::GetUpTime() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(upTimeCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }
    
    return static_cast<uint32_t>(value.longValue);
}

uint32_t ProcessMonitorPrivate::GetHandles() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(handlesCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t ProcessMonitorPrivate::GetThreads() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(threadsCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t ProcessMonitorPrivate::GetPriorityBase() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(priorityBaseCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint8_t ProcessMonitorPrivate::GetProcessorTime() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(processorTimeCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint8_t>(value.longValue);
}

uint8_t ProcessMonitorPrivate::GetMemoryUsagePercent() const
{
    const uint64_t total = static_cast<uint64_t>(memoryStatus_.ullTotalPhys);
    const uint64_t used = GetMemoryUsageBytes();

    return static_cast<uint8_t>(rint((used * 100) / total));
}

uint32_t ProcessMonitorPrivate::GetMemoryUsageBytes() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(memoryCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint8_t ProcessMonitorPrivate::GetVirtMemoryUsagePercent() const
{
    const uint64_t total = static_cast<uint64_t>(memoryStatus_.ullTotalVirtual);
    const uint64_t used = GetVirtMemoryUsageBytes();

    return static_cast<uint8_t>(rint((used * 100) / total));
}

uint32_t ProcessMonitorPrivate::GetVirtMemoryUsageBytes() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(virtMemoryCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint8_t ProcessMonitorPrivate::GetSwapUsagePercent() const
{
    const uint64_t total = static_cast<uint64_t>(memoryStatus_.ullTotalPageFile);
    const uint64_t used = GetSwapUsageBytes();

    return static_cast<uint8_t>(rint((used * 100) / total));
}

uint32_t ProcessMonitorPrivate::GetSwapUsageBytes() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(swapCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t ProcessMonitorPrivate::GetIOOperationsSec() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(ioOperationsSecCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}

uint32_t ProcessMonitorPrivate::GetIOBytesSec() const
{
    PDH_STATUS error = ERROR_SUCCESS;
    PDH_FMT_COUNTERVALUE value = { 0 };

    error = ::PdhGetFormattedCounterValue(ioBytesSecCounter_, PDH_FMT_LONG, nullptr, &value);
    if (error != ERROR_SUCCESS) {
        //throw system_error(error, system_category());
    }

    return static_cast<uint32_t>(value.longValue);
}


string ProcessMonitorPrivate::GetProcessNameByPid(int32_t pid)
{
    HANDLE proc = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
    if (proc == nullptr) {
        throw system_error(::GetLastError(), system_category());
    }
    
    string path(MAX_PATH, '\0');
    if (::GetProcessImageFileNameA(proc, &path[0], path.size()) == 0) {
        throw system_error(::GetLastError(), system_category());
    }

    ::CloseHandle(proc);

    string name = ::PathFindFileNameA(path.data());
    ::PathRemoveExtensionA(&name[0]);
    
    return name;
}

string ProcessMonitorPrivate::GetProcessCounterPath(const string& process, const string& counter)
{
    PDH_COUNTER_PATH_ELEMENTS_A cpe = { 0 };
    cpe.szObjectName = "Process";
    cpe.szInstanceName = const_cast<PSTR>(process.data());
    cpe.dwInstanceIndex = -1;
    cpe.szCounterName = const_cast<PSTR>(counter.data());

    string path(MAX_PATH, '\0');
    DWORD size = path.size();
    PDH_STATUS error = ERROR_SUCCESS;
    error = ::PdhMakeCounterPathA(&cpe, &path[0], &size, 0);
    if (error != ERROR_SUCCESS) {
        throw system_error(error, system_category());
    }

    return path;
}

} // namespace testtools
